build: ## Construye la imagen con podman
	podman build -t mi-express-server_mi-express .
run:  ## Lanza el contenedorcon podman y entra en bash 
	podman run -p 3000:3000 -v ./:/src/app -it localhost/mi-express-server_mi-express bash
build-compose: ## Construye la imagen con podman-compose
	podman-compose build
run-compose:  ## Arranca el contenedor con podman-compose run y entra en bash
	podman-compose run --service-ports --rm --name mi-express bash
up-attach:  ## Arranca el contenedor con podman-compose up. Puede tener problemas en la shell 
	podman-compose --podman-run-args="-t -i " up
up:  ## Arranca el contenedor con podman-compose up , haciendo run -d en lugar de  create para solucionar el problea con la shell de up
	podman-compose --podman-run-args="-t -i " up -d
	podman start -a mi-express
start: ## Entra en un contenedor creado previamente
	podman start -a mi-express
logs:
	podman-compose logs -f
stop: ## Para y borra el contenedor
	podman-compose down --remove-orphans
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
ps-compose:
	podman-compose ps

# PROYECTO BÁSICO PARA PROBAR  NODEJS + EXPRESS + EJECUTAR EN PODMAN

Este ejeplo pretende demostrar cómo trabajar con nodejs con runtime en un contenedor ejecutado en local 
 
Está basado en https://platformengineer.com/podman-run-node-js-express-server/

# DOCUMENTACION ADICIONAL 

- podman-compose:  
    https://github.com/containers/podman-compose  
    https://compose-spec.io/  

# VENTAJAS DE ESTE MODO DE DESARROLLO

Se trata de trabajar con runtimes independientes en cada proyecto. La aplicación se ejecuta dentro de un contenedor en el que se ha instalado una versión del runtime ( en estecaso ,nodejs, pero peude ser java, spring boot..).
Al ejecutarse dentro de un contenedor, está aislado y no interfiere con nuestro sistema operativo y las versiones de runtimes que podamos tener instaladas.


# PRERREQUISITOS

Tener instalado:


WSL2: entorno linux en windows en el que se puede instalar podman. La ventaja de este entorno es que no hace falta ser adminsitrador Alternativas: máquina linux, virtualBox, docker/podman para windows...

Dentro de WSL2:

    - podman
    - podman-compose
    - make

VSCODE : para el código fuente. Además, puede abrir directamente una terminal con WSL2

# USO

1) Inspeccionar el Dockerfile. Ver que ejecuta los pasos necesarios para poder ejecutar npm con los proxies de SELAE:

- ejecutar npm install express para descargar todas las dependencias de la aplicación. Lo instala en global, para que al montar el dir local como volumen, no sustituya al contenido de la imagen. 

2) construir la imagen con podman

podman build -t mi-express .

Para el trabajo normal se facilita con cualquiera de estos comandos make:

make build

make build-compose

ver docker-compose.yaml para más infromación


3) ejecutar el contenedor

podman run -p 3000:3000 -it localhost/app-image bash

El flujo de trabajo normal es utilizar estos comandos:

make up 

make stop

o bien:

make run-compose

make stop


4) levantar la aplicación, abre un bash  para poder trabajar en el contenedor con npm. Dentro del contenedor:

npm start

## NOTA SOBRE LA ADAPTACION DE PODMAN-COMPOSE:

podman-compose aplica una política para crear o hacer referencia a las imangenes:

- si tenemos el container_name fijo , entonces para la imagen utiliza el directorio parent como prefijo
- si no, le añade un sufijo aleatorio

Para mantener la consistencia entre todos los comandos se ha adoptado esta convención:

nombre de la imagen: prefijo + container_name , es decir: imagen_mi-express  
nombre  del contenedor: container_name , es decir: mi-express

se implementa pasando el param -p imagen (el prefijo) a podman-compose.

Alternativamente , si preferimos no usar el param -p, tener en cuenta que los comandos podman van a crear una imagen con nombre diferente. Si queremos utilizar indistintamente make build y make build-compose, hay que incoporar el dir actual como prefijo con _
al nombre de la imagen. Asi es como está en este proyecto:

podman build -t mi-express-server_mi-express .

podman-compose build

podman-compose run --service-ports --rm --name mi-express bash

# Comentarios y mejoras

Sería preferible utilizar un directorio distinto a /src/app como punto de montaje dentro del contenedor. Este puede llevar a confusión por ejemplo en proyectos con angular, donde tendríamos el código en /src/app/src/app o /src/app/subproyecto/src/app. 


Se pueden hacer 2 imagenes: una "init" para instalar las deps y otra para arrancar.

Un ejemplo, aquí:

https://github.com/containers/podman-compose/blob/devel/examples/nodeproj/docker-compose.yml

Alternativamente se puede instalar el node_modules en local. Eso es lo que pasará en proyectos más complejos. Se pueden buscar otra formas, pero tener en cuenta siempre que al arrancar montando el directorio local en el contenedor en /src/app, lo que hubiera ahí en la imagen se pierde.

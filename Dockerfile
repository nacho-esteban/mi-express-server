FROM node
RUN mkdir -p /src/app 
WORKDIR /src/app
# necesario para que node sepa donde estan los paquetes instalados en global
ENV NODE_PATH=/usr/local/lib/node_modules
RUN npm install -g express